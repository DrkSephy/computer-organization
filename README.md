# Computer Organization

Labs done using VHDL, as well as Quartus and ModelSim. Labs 3-8 were simulated on the DE2 board through the use of Quartus. 

###Labs
---
* Multiplexors and Adders
* Flip-flops and Latches
* Introduction to Quartus
* Fast Adder
* Keyboard
* Mouse
* VGA
* CPU
