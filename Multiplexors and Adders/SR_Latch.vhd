--SR-Latch code

Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SR_Latch is
	port(
		Set, Reset	: in STD_LOGIC;
		Q, notQ		: inout STD_LOGIC;
end SR_Latch;

architecture arch of SR_Latch is
begin
		Q <= Reset nor notQ;
		notQ <= Set nor Q;
end arch;