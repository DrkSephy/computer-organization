--This file fixes all bugs in the first two files
--(we were setting "expectedOutput" incorrectlyin the first file)
--(we were setting currentA to be equal to i instead of j in the second file)

--More importantly, it adds wait statements to allow our signals to propagate
--Note that in this class, test benches are the only time you
--should be using wait statements and for loops

--if you have these types of code in your circuit implementations, you 
--are not implementing them correctly

Library ieee;
Use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Entity lecture2_test3 is
  -- empty, since this is just a test bench
End lecture2_test3;

Architecture arch of lecture2_test3 is

component eight_bit_comparator 
Port (
	InputA, InputB: in std_logic_vector(7 downto 0);
	AEqualsB    : out std_logic);
End component; 

	
Signal actualOutput, expectedOutput	: std_logic;
Signal currentA, currentB  : std_logic_vector(7 downto 0);

procedure test_values is begin
	wait for 10ns;
	if(not(actualOutput = expectedOutput)) then
		report "TEST FAILED! The simulation is now halted." severity failure;
	end if;
	wait for 10ns;
end procedure test_values;

begin

my_test_eight_bit_comp : eight_bit_comparator port map(
	InputA => currentA,
	InputB => currentB,
	AEqualsB => actualOutput
);
	
test_process : process begin
	for i in 0 to 255 loop
		for j in 0 to 255 loop
			currentA <= std_logic_vector(to_unsigned(i, currentA'length));
			currentB <= std_logic_vector(to_unsigned(j, currentB'length));
			if (i = j) then
				expectedOutput <= '1';
			else
				expectedOutput <= '0';  
			end if;

			test_values;
		end loop;
	end loop;

	report "None! TEST SUCCESSFUL! End of simulation." severity failure;
end process test_process;

End arch;


