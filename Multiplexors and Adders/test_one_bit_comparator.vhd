Library ieee;

use ieee.std_logic_1164.all;

--our test script doesn't expose anything to the outside world,
--so we have an empty entity declaration
Entity test_one_bit_comparator is
End test_one_bit_comparator; 

Architecture arch of test_one_bit_comparator is

--a component declaration defines a black box that we are using
--from another file or architecture
--in this case, since we are testing our comparator, we need
--to declare this type of component
--make sure the signal parameters matchthe other file exactly
component one_bit_comparator
Port (
	InputA, InputB : in std_logic;
	AEqualsB : out std_logic);
End component;



--these internal signals are used to "wire up" our 
--comparator for testing
Signal currentA, currentB, expectedResult, actualResult : std_logic;

--this is a helper function that we use to test each of the
--four possible cases. Breaking this logic into a separate 
--process reduces the amount of copy/paste in the main test process
procedure test_values is begin

	--test benches will wait to allow signals to propagate
	--it makes the graphs easier to read too
	wait for 25 ns;

	--we check if the result from our comparator matches
	--what we expected to get
	if (not (actualResult = expectedResult)) then
	--if it doesn't, we halt with an appropriate message
		report "TEST FAILED! Simulation halted" severity failure;
	end if;
	
	--we wait again, just to make the graph easier to read
	wait for 25 ns;
end procedure test_values;  


begin --begin our architecture
  
--a port map allows us to connect multiple "black box" style
--components together. Here we instantiate and connect
--one comparator circuit for testing
--note port maps use => rather than <=
my_comparator : one_bit_comparator port map (
	InputA => currentA, 
	InputB => currentB, 
	AEqualsB => actualResult
); 


--finally we create our main test process, which goes through each
--of our test cases, invoking the helper process to verify each case
test_process : process begin
	currentA <= '0';
	currentB <= '0';	
	expectedResult <= '1';
	test_values;
	
	currentA <= '0';
	currentB <= '1';	
	expectedResult <= '0';
	test_values;
	
	currentA <= '1';
	currentB <= '0';	
	expectedResult <= '0';
	test_values;
	
	currentA <= '1';
	currentB <= '1';	
	expectedResult <= '1';
	test_values;

	--If we get here, we have passed all the tests, so 
	--we report success. It is a little confusing that
	--we use the "severity failure" statement. However this just tells
	--ModelSIM that we are done testing. Pay more attention to the
	--message than the severity. This is the only way to halt a 
	--process in VHDL 
	report "NONE. TEST SUCCESSFUL! End of simulation." severity failure;
end process test_process;
  
End arch;
