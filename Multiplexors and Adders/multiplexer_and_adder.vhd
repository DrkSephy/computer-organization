LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Multiplexer_4_to_1_four_bit is
port (
	I0, I1, I2, I3 : in STD_LOGIC_Vector(3 Downto 0);
	S : in STD_LOGIC_Vector(1 Downto 0);
	Output : out STD_LOGIC_Vector(3 Downto 0)
);
end Multiplexer_4_to_1_four_bit;

architecture behav of Multiplexer_4_to_1_four_bit is
begin
  Process (S)
  Begin
    --Enter your code here
    If (S(0) = '0' and S(1) = '0') then
    	Output <= I0;
    elsif (S(0) = '0' and S(1) = '1') then
    	Output <= I1;
    elsif (S(0) = '1' and S(1) = '0') then
    	Output <= I2;
    elsif (S(0) = '1' and S(1) = '1') then
    	Output <= I3;
    end if;
  end process;
end behav;


LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Multiplexer_and_Adder is
port (
	A0, A1, A2, A3, B0, B1, B2, B3: in STD_LOGIC_Vector(3 Downto 0);
	SA, SB : in STD_LOGIC_Vector(1 Downto 0);
	S : out STD_LOGIC_Vector(3 Downto 0);
	Cout : out STD_LOGIC
);
end Multiplexer_and_Adder;

architecture arch of Multiplexer_and_Adder is
Component Multiplexer_4_to_1_four_bit
port (
	I0, I1, I2, I3 : in STD_LOGIC_Vector(3 Downto 0);
	S : in STD_LOGIC_Vector(1 Downto 0);
	Output : out STD_LOGIC_Vector(3 Downto 0)
);
end Component;

Component Four_Bit_Adder
Port (
	I0,I1 : in STD_LOGIC_VECTOR(3 Downto 0);
	Cin : in STD_LOGIC;
	S : out STD_LOGIC_VECTOR(3 Downto 0);
	Cout : out STD_LOGIC
);
End Component;

Signal AdderInput0, AdderInput1 : STD_LOGIC_Vector(3 Downto 0);
begin
Mux1: Multiplexer_4_to_1_four_bit Port map (
	I0 => A0,
	I1 => A1,
	I2 => A2,
	I3 => A3,
	S => SA,
	Output => AdderInput0
);
Mux2: Multiplexer_4_to_1_four_bit Port map (
	--Enter your code here
	I0 => B0,
	I1 => B1,
	I2 => B2,
	I3 => B3,
	S => SB,
	Output => AdderInput1
);

FA: Four_Bit_Adder port map (
	--Enter your code here
	I0 => AdderInput0,
	I1 => AdderInput1,
	Cin => '0',
	S => S,
	Cout => Cout
);
end arch; 
