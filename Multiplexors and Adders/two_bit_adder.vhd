Library ieee;
Use ieee.std_logic_1164.all;
Entity Two_Bit_Adder is
Port (
	A0, A1, B0, B1 : in STD_LOGIC;
	Cin : in STD_LOGIC;
	S0, S1 : out STD_LOGIC;
	Cout : out STD_LOGIC
);
End Two_Bit_Adder;

Architecture arch of Two_Bit_Adder is
Component Full_Adder
Port (
	A, B, Cin : in std_logic;
	S, Cout : out std_logic
);
End Component;

Signal TempCout: std_logic;
begin
FA1: Full_Adder port map (
	A => A0,
	B => B0,
	Cin => Cin,
	S => S0,
	Cout => TempCout
);
FA2: Full_Adder port map (
	B => B1,
	A => A1,
	Cin => TempCout,
	S => S1,
	Cout => Cout
);
End arch;
