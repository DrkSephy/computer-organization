Library ieee;
use ieee.std_logic_1164.all;

Entity Test_Multiplexer_and_Adder is
End Test_Multiplexer_and_Adder;


Architecture arch of Test_Multiplexer_and_Adder is
component Multiplexer_and_Adder
	port(
		A0, A1, A2, A3, B0, B1, B2, B3	: in STD_LOGIC_Vector (3 Downto 0);
		SA, SB							: in STD_LOGIC_Vector (1 Downto 0);
		S								: out STD_LOGIC_Vector (3 Downto 0);
		Cout							: out STD_LOGIC);
end component;

Signal A0, A1, A2, A3, B0, B1, B2, B3, S: STD_LOGIC_Vector (3 Downto 0);
Signal SA, SB							: STD_LOGIC_Vector (1 downto 0);
Signal Cout								: STD_LOGIC;
Signal error							: STD_LOGIC := '0';
begin
	MA: Multiplexer_and_Adder port map(
		A0 => A0,
		A1 => A1,
		A2 => A2,
		A3 => A3,
		B0 => B0,
		B1 => B1,
		B2 => B2,
		B3 => B3,
		SA => SA,
		SB => SB,
		S => S,
		Cout => Cout);
		
		A0 <= "0010";
		A1 <= "0011";
		A2 <= "0100";
		A3 <= "0101";
		B0 <= "1000";
		B1 <= "0111";
		B2 <= "0110";
		B3 <= "0101";
		SB <= SA;
--test base cases /concatenate the size of the tests	
process	begin
		SA <= "00";
		wait for 1 ns;
    if (S /= "1010") then
    	error <= '1';
    end if;
    	wait for 100 ns;
    	SA <= "01";
    	wait for 1 ns;
    if (S /= "1010") then
    	error <= '1';
    end if;
    	wait for 100 ns;
    	SA <= "10";
    	wait for 1 ns;
    if (S /= "1010") then
    	error <= '1';
    end if;
    	wait for 100 ns;
    	SA <= "11";
    	wait for 1 ns;
    if (S /= "1010") then
    	error <= '1';
    end if;
    	wait for 100 ns;
    
if (error ='0') then
		report "No errors found. Simulation successful" severity failure;
	else
		report "Error(s) found." severity failure;
end if;
	end process;

end arch;
    
