--Mux_RGB
--When Object_1_On is '1', pixel gets sent to region inside object
--When Object_1_On is '0', pixel gets sent to background
--When video_on is '0', output == 0

library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 

entity Mux_RGB is
	port(
		Object_1_On, Video_On 			: in std_logic;
		Background_RGB, Object_1_RGB 	: in std_logic_vector(2 downto 0);
		RGB								: out std_logic_vector(2 downto 0)
		);
end Mux_RGB;

architecture arch of Mux_RGB is

signal r: std_logic_vector(2 downto 0);

begin
	--enter your code here
	RGB <= "000" when Video_On = '0'
	else Object_1_RGB when Object_1_On = '1'
	else Background_RGB;
end arch;
