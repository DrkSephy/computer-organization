--position controller
--Moves the object in the direction designated by specific switch on board

library ieee; 
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all; 

entity Pos_Controler is
port (
	L, R, U, D, Move, Reset: in std_logic; 
	Init_X, Init_Y: out std_logic_vector(9 downto 0)
);
end Pos_Controler;

architecture arch of Pos_Controler is
signal x, y: unsigned(9 downto 0):= "0011001000";
begin
Init_X <= std_logic_vector(x); 
Init_Y <= std_logic_vector(Y);

Process(Move, reset) 
begin
--Enter your code here
	if(reset = '1') then
		x <= "0000000000";
		y <= "0000000000";
	elsif(rising_edge(Move)) then
	--Left switch on, shift to right by 4
		if(L ='1' and x > 3) then
			x <= x -4;
		end if;
	--Right switch on, shift to left by 4
		if(R = '1' and x < 637) then
			x <= x + 4;
		end if;
	--Up switch on, shift up by 4
		if(U = '1' and y > 3) then
			y <= y - 4;
		end if;
	--Down switch on, shift down by 5
		if(D = '1' and y < 477) then
			y <= y + 4;
		end if;
	end if;
	
	end process;
end arch;