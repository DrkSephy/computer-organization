--Object.vhd
--Displays the 208 x 64 pixel rectangle
--We have to determine if the location is in the region of the rectangle
--When it is, we set the RGB output to the color of the object
--And set object_on = '1'

library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

entity Object is
port (
	Init_X, Init_Y: in std_logic_vector(9 downto 0); 
	Pixel_X, Pixel_Y: in std_logic_vector(9 downto 0); 
	RGB : out std_logic_vector(2 downto 0);
	Object_On: out std_logic
	);
end object;

architecture arch of Object is

Constant W : unsigned(9 downto 0):= "0011010000"; --208 C
Constant H : unsigned(9 downto 0):= "0001000000"; --64 
signal Color : std_logic_vector(2 downto 0):= "010"; --Green (You can change this if you like)

begin

process(Pixel_X, Pixel_Y)
begin
	--enter your code here
	--Test if pixels are within the screen boundaries
	
	if((unsigned(Pixel_X) <= (W + unsigned(Init_X)) and Pixel_X >= Init_X)
		and (unsigned(Pixel_Y) <= (H + unsigned(Init_Y)) and Pixel_X >= Init_Y)) then
	
	--If the pixels are in the boundary of the screen, color that pixel
		RGB <= color;
	--Draw pixel
		Object_On <= '1';
	else
	--Out of boundary, do not draw pixel
		Object_On <= '0';
	end if;
	
	end process;
end arch;