-- test Multiplexer

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_control_unit is
end test_control_unit;

Architecture arch_test of test_control_unit is

Component control_unit port(
	run, reset, clock : in std_logic;
	sel				:out std_logic_vector(3 downto 0);
	done, regA_en, regG_en, IR_en, minus_sign :out std_logic;
	reg0_en, reg1_en, reg2_en, reg3_en, reg4_en, reg5_en, reg6_en, reg7_en : out std_logic;
	instruction	:in std_logic_vector(11 downto 0)
	);
end component;

signal run, reset, clock : std_logic;
signal sel : std_logic_vector(3 downto 0);
signal done, regA_en, regG_en, IR_en, minus_sign : std_logic;
signal reg0_en, reg1_en, reg2_en, reg3_en, reg4_en, reg5_en, reg6_en, reg7_en : std_logic;
signal instruction : std_logic_vector(11 downto 0);
signal IR, XXXX, YYYY : std_logic_vector(3 downto 0);

constant mov : std_logic_vector(3 downto 0) := "0000";
constant movi : std_logic_vector(3 downto 0) := "0001";
constant add : std_logic_vector(3 downto 0) := "0010";
constant sub : std_logic_vector(3 downto 0) := "0011";
constant Din : std_logic_vector(3 downto 0) := "1000";
constant regG : std_logic_vector(3 downto 0) := "1001";

signal error: std_logic:= '0';
signal test_num: std_logic_vector(3 downto 0):= "0000";
begin

instruction(11 downto 8) <= IR;
instruction(7 downto 4) <= XXXX;
instruction (3 downto 0) <= YYYY;

cu: control_unit port map
(
	clock => clock,
	run => run,
	reset => reset,
	sel => sel,
	reg0_en => reg0_en,
	reg1_en => reg1_en,
	reg2_en => reg2_en,
	reg3_en => reg3_en,
	reg4_en => reg4_en,
	reg5_en => reg5_en,
	reg6_en => reg6_en,
	reg7_en => reg7_en,
	regA_en => regA_en,
	regG_en => regG_en,
	minus_sign => minus_sign,
	IR_en => IR_en,
	done => done,
	instruction => instruction
);

process begin
wait for 50 ns;
	run <= '1';
	reset <= '0';
	clock <= '0';
wait for 50 ns;

-------------------------------- test MOV -----------------------------------
IR <= mov;

for i in 0 to 7 loop
XXXX <= std_logic_vector(to_unsigned(i, 4));
for k in 0 to 7 loop
YYYY <= std_logic_vector(to_unsigned(k, 4));
clock <= '1'; -- get opcode
wait for 50 ns;
clock <= '0';
wait for 50 ns;
clock <= '1'; -- mov operation
wait for 50 ns;
if(sel /= YYYY or done /= '1' ) then
	error <= '1';
end if;
	-- test registers
	case XXXX is
		when "0000" =>
			if (reg0_en /= '1') then
				error <= '1';
			end if;
		when "0001" =>
			if (reg1_en /= '1') then
				error <= '1';
			end if;
		when "0010" =>
			if (reg2_en /= '1') then
				error <= '1';
			end if;
		when "0011" =>
			if (reg3_en /= '1') then
				error <= '1';
			end if;
		when "0100" =>
			if (reg4_en /= '1') then
				error <= '1';
			end if;
		when "0101" =>
			if (reg5_en /= '1') then
				error <= '1';
			end if;
		when "0110" =>
			if (reg6_en /= '1') then
				error <= '1';
			end if;
		when "0111" =>
			if (reg7_en /= '1') then
				error <= '1';
			end if;
		when others =>
			-- do nothing
	end case;
wait for 50 ns;								
clock <= '0';
wait for 50 ns;
clock <= '1'; -- back to idle
wait for 50 ns;
clock <= '0';
end loop;
end loop;

	-- report test results
	if (error = '1') then
		report "Error TEST MOV" severity failure;
	end if;
	

-------------------------------- test MOVI ----------------------------------
test_num <= "0001";
wait for 50 ns;
IR <= movi;

for i in 0 to 7 loop
XXXX <= std_logic_vector(to_unsigned(i, 4));
for k in 0 to 7 loop
YYYY <= std_logic_vector(to_unsigned(k, 4));
wait for 50 ns;
clock <= '1'; -- movi operation
wait for 50 ns;
if(sel /= Din or done /= '1') then	
	error <= '1';
end if;
	-- test register X
	case XXXX is
		when "0000" =>
			if (reg0_en /= '1') then
				error <= '1';
			end if;
		when "0001" =>
			if (reg1_en /= '1') then
				error <= '1';
			end if;
		when "0010" =>
			if (reg2_en /= '1') then
				error <= '1';
			end if;
		when "0011" =>
			if (reg3_en /= '1') then
				error <= '1';
			end if;
		when "0100" =>
			if (reg4_en /= '1') then
				error <= '1';
			end if;
		when "0101" =>
			if (reg5_en /= '1') then
				error <= '1';
			end if;
		when "0110" =>
			if (reg6_en /= '1') then
				error <= '1';
			end if;
		when "0111" =>
			if (reg7_en /= '1') then
				error <= '1';
			end if;
		when others =>
			-- do nothing
	end case;
wait for 50 ns;								
clock <= '0';
wait for 50 ns;
clock <= '1'; -- back to idle
wait for 50 ns;
clock <= '0';
end loop;
end loop;

	-- report test results
	if (error = '1') then
		report "Error TEST MOVI" severity failure;
	end if;

-------------------------------- test ADD -----------------------------------
test_num <= "0010";
wait for 50 ns;
IR <= add;

for i in 0 to 7 loop
XXXX <= std_logic_vector(to_unsigned(i, 4));
for k in 0 to 7 loop
YYYY <= std_logic_vector(to_unsigned(k, 4));

wait for 50 ns;
clock <= '1'; -- opcode to add transition
wait for 50 ns;
clock <= '0';
wait for 50 ns;
if(sel /= XXXX or regA_en /= '1' or done /= '0' or IR_en /= '0') then
	error <= '1';
else
	report " Entering Add/Sub state";
end if;
								
clock <= '1'; -- addition
wait for 50 ns;
clock <= '0';
wait for 50 ns;
if(sel /= YYYY or regA_en /= '0' or regG_en /= '1' or minus_sign /= '0' or done /= '0' or IR_en /= '0') then
	error <= '1';
	report "Entering Store Result";
end if;
		
clock <= '1'; -- back to idle
wait for 50 ns;
clock <= '0';
wait for 50 ns;
if(sel /= regG or regA_en /= '0' or regG_en /= '0' or minus_sign /= '0' or done /= '1' or IR_en /= '0') then
	error <= '1';
	report "Going Back to Idle";
end if;

	-- test register X
	report "testing register X"; 
	case XXXX is
		when "0000" =>
			if (reg0_en /= '1') then
				error <= '1';
			end if;
		when "0001" =>
			if (reg1_en /= '1') then
				error <= '1';
			end if;
		when "0010" =>
			if (reg2_en /= '1') then
				error <= '1';
			end if;
		when "0011" =>
			if (reg3_en /= '1') then
				error <= '1';
			end if;
		when "0100" =>
			if (reg4_en /= '1') then
				error <= '1';
			end if;
		when "0101" =>
			if (reg5_en /= '1') then
				error <= '1';
			end if;
		when "0110" =>
			if (reg6_en /= '1') then
				error <= '1';
			end if;
		when "0111" =>
			if (reg7_en /= '1') then
				error <= '1';
			end if;
		when others =>
			-- do nothing
	end case;
clock <= '0';
wait for 50 ns;
clock <= '1'; -- back to idle
wait for 50 ns;
clock <= '0';
end loop;
end loop;

	-- report test results
	if (error = '1') then
		report "Error TEST ADD" severity failure;
	else
		report "ADD passed";
	end if;

-------------------------------- test SUB -----------------------------------
test_num <= "0011";
wait for 50 ns;
IR <= sub;

for i in 0 to 7 loop
XXXX <= std_logic_vector(to_unsigned(1, 4));
for k in 0 to 7 loop
YYYY <= std_logic_vector(to_unsigned(3, 4));

wait for 50 ns;
clock <= '1'; -- opcode to add transition
wait for 50 ns;
clock <= '0';
wait for 50 ns;
if(sel /= XXXX or regA_en /= '1' or done /= '0' or IR_en /= '0') then
	error <= '1';
else
	report " Entering Add/Sub state";
end if;
								
clock <= '1'; -- addition
wait for 50 ns;
clock <= '0';
wait for 50 ns;
if(sel /= YYYY or regA_en /= '0' or regG_en /= '1' or minus_sign /= '1' or done /= '0' or IR_en /= '0') then
	error <= '1';
	report "Entering Store Result";
end if;
		
clock <= '1'; -- back to idle
wait for 50 ns;
clock <= '0';
wait for 50 ns;
if(sel /= regG or regA_en /= '0' or regG_en /= '0' or minus_sign /= '1' or done /= '1' or IR_en /= '0') then
	error <= '1';
	report "Going Back to Idle";
end if;

	-- test register X
	report "testing register X"; 
	case XXXX is
		when "0000" =>
			if (reg0_en /= '1') then
				error <= '1';
			end if;
		when "0001" =>
			if (reg1_en /= '1') then
				error <= '1';
			end if;
		when "0010" =>
			if (reg2_en /= '1') then
				error <= '1';
			end if;
		when "0011" =>
			if (reg3_en /= '1') then
				error <= '1';
			end if;
		when "0100" =>
			if (reg4_en /= '1') then
				error <= '1';
			end if;
		when "0101" =>
			if (reg5_en /= '1') then
				error <= '1';
			end if;
		when "0110" =>
			if (reg6_en /= '1') then
				error <= '1';
			end if;
		when "0111" =>
			if (reg7_en /= '1') then
				error <= '1';
			end if;
		when others =>
			-- do nothing
	end case;
clock <= '0';
wait for 50 ns;
clock <= '1'; -- back to idle
wait for 50 ns;
clock <= '0';
end loop;
end loop;

	-- report test results
	if (error = '1') then
		report "Error TEST SUB" severity failure;
	end if;


-------------------------------- test SUB -----------------------------------
test_num <= "0100";
run <= '0';
wait for 50 ns;
clock <= '1';
wait for 50 ns;
clock <= '0';
wait for 50 ns;
if( reg0_en /= '0' or reg1_en /= '0' or reg2_en /= '0' or reg3_en /= '0' or reg4_en /= '0' 
	or reg5_en /= '0' or reg6_en /= '0'  or reg7_en /= '0' or IR_en /= '0' ) then
	report "Error TEST RUN" severity failure;
end if;
	
-------------------------------- test RESET -----------------------------------
test_num <= "0101";
run <= '1';
reset <= '1';
wait for 50 ns;
clock <= '1';
wait for 50 ns;
clock <= '0';
wait for 50 ns;
if( reg0_en /= '0' or reg1_en /= '0' or reg2_en /= '0' or reg3_en /= '0' or reg4_en /= '0' 
	or reg5_en /= '0' or reg6_en /= '0'  or reg7_en /= '0' or IR_en /= '1' ) then
	report "Error TEST RESET" severity failure;
end if;


	-- report test results
	if (error = '0') then
		report "No errors detected. Simulation successful" severity failure;
	else
		report "Error detected" severity failure;
	end if;
	
end process;
end arch_test;