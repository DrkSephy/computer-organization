--CPU code

library ieee;
use ieee.std_logic_1164.all;

entity CPU is
	port(
		Run		:in std_logic;
		Reset	:in std_logic;
		Clock	:in std_logic;
		Din		:in std_logic_vector(15 downto 0);
		Done	:out std_logic;
		BusLine	:out std_logic_vector(15 downto 0);
		);
end CPU;

architecture struct of CPU is

component Reg16 is
	port(
		clock		:in std_logic;
		enable		:in std_logic;
		D			:in std_logic_vector(15 downto 0);
		Q			:out std_logic_vector(15 downto 0)
		);
end component;

component InReg is
	port(
		clock		:in std_logic;
		enable		:in std_logic;
		Din			:in std_logic_vector(15 downto 0);
		Instruction	:out std_logic_vector(11 downto 0)
		);
end component;

component Multiplexer is
	port(
	sel				:in std_logic_vector(3 downto 0);
	reg0_in			:in std_logic_vector(15 downto 0);
	reg1_in			:in std_logic_vector(15 downto 0);
	reg2_in			:in std_logic_vector(15 downto 0);
	reg3_in			:in std_logic_vector(15 downto 0);
	reg4_in			:in std_logic_vector(15 downto 0);
	reg5_in			:in std_logic_vector(15 downto 0);
	reg6_in			:in std_logic_vector(15 downto 0);
	reg7_in			:in std_logic_vector(15 downto 0);
	Din				:in std_logic_vector(15 downto 0);
	regG_in			:in std_logic_vector(15 downto 0);
	bus_line		:out std_logic_vector(15 downto 0)
		);
end component;

component Adder is
	port(
		minus_sign	:in std_logic;
		Rx			:in std_logic_vector(15 downto 0);
		Ry			:in std_logic_vector(15 downto 0);
		result		:out std_logic_vector(15 downto 0)
		);
end component;

component control_unit is
	port(
		run			:in std_logic;
		reset		:in std_logic;
		clock		:in std_logic;
		sel			:out std_logic_vector(3 downto 0);
		done		:out std_logic;
		regA_en		:out std_logic;
		regG_en		:out std_logic;
		IR_en		:out std_logic;
		minus_sign	:out std_logic;
		reg0_en		:out std_logic;
		reg1_en		:out std_logic;
		reg2_en		:out std_logic;
		reg3_en		:out std_logic;
		reg4_en		:out std_logic;
		reg5_en		:out std_logic;
		reg6_en		:out std_logic;
		reg7_en		:out std_logic;
		instruction	:in std_logic_vector(11 downto 0)
		);
end component;

signal reg0_en, reg1_en, reg2_en, reg3_en, reg4_en, reg5_en, reg6_en, reg7_en : std_logic;
signal regA_en, regG_en, IR_en, minus_sign : std_logic;
signal sel : std_logic_vector(3 downto 0);
signal reg0_signal, reg1_signal, reg2_signal, reg3_signal, reg4_signal, reg5_signal, reg6_signal, reg7_signal : std_logic_vector(15 downto 0);
signal regA_signal, regG_signal, sum, bus_signal : std_logic_vector(15 downto 0);
signal instruction : std_logic_vector(11 downto 0);

begin 
BusLine <= bus_signal;

cu: control_unit port map (
	clock => clock,
	run => run,
	reset => reset,
	sel => sel,
	reg0_en => reg0_en,
	reg1_en => reg1_en,
	reg2_en => reg2_en,
	reg3_en => reg3_en,
	reg4_en => reg4_en,
	reg5_en => reg5_en,
	reg6_en => reg6_en,
	reg7_en => reg7_en,
	regA_en => regA_en,
	regG_en => regG_en,
	minus_sign => minus_sign,
	IR_en => IR_en,
	done => done,
	instruction => instruction
);

mux : multiplexer port map (
	sel	=> sel,
	reg0_in	=> reg0_signal,
	reg1_in	=> reg1_signal,
	reg2_in	=> reg2_signal,
	reg3_in	=> reg3_signal,
	reg4_in	=> reg4_signal,
	reg5_in	=> reg5_signal,
	reg6_in	=> reg6_signal,
	reg7_in	=> reg7_signal,
	Din	=> Din,
	regG_in	=> regG_signal,
	bus_line => bus_signal
);

adder_subtracter : 	Adder port map (
	minus_sign => minus_sign,
	Rx => regA_signal,
	Ry => bus_signal,
	result => regG_signal
);

regA : reg16 port map (
		clock => clock,
		enable => regA_en,
		D => bus_signal,
		Q => regA_signal
);
regG : reg16 port map (
		clock => clock,
		enable => regG_en,
		D => sum,
		Q => regG_signal
);
IRreg : InReg port map (
		clock => clock,
		enable => IR_en,
		Din	=> Din,
		Instruction	=> instruction
);
reg0 : reg16 port map (
		clock => clock,
		enable => reg0_en,
		D => bus_signal,
		Q => reg0_signal
);
reg1 : reg16 port map (
		clock => clock,
		enable => reg1_en,
		D => bus_signal,
		Q => reg1_signal
);
reg2 : reg16 port map (
		clock => clock,
		enable => reg2_en,
		D => bus_signal,
		Q => reg2_signal
);
reg3 : reg16 port map (
		clock => clock,
		enable => reg3_en,
		D => bus_signal,
		Q => reg3_signal
);
reg4 : reg16 port map (
		clock => clock,
		enable => reg4_en,
		D => bus_signal,
		Q => reg4_signal
);
reg5 : reg16 port map (
		clock => clock,
		enable => reg5_en,
		D => bus_signal,
		Q => reg5_signal
);
reg6 : reg16 port map (
		clock => clock,
		enable => reg6_en,
		D => bus_signal,
		Q => reg6_signal
);
reg7 : reg16 port map (
		clock => clock,
		enable => reg7_en,
		D => bus_signal,
		Q => reg7_signal
);

end struct;