--Instruction register
--Ignores first 4 leading bits, takes next 12 bits
--Format is 0000IIIIXXXXYYYY

library ieee;
use ieee.std_logic_1164.all;

entity InReg is
	port(
		clock		:in std_logic;
		enable		:in std_logic;
		Din		:in std_logic_vector(15 downto 0);
		Instruction	:out std_logic_vector(11 downto 0)
	);
end InReg;

architecture struct of InReg is
	begin
		process(clock)
			begin
				if(rising_edge(clock) and enable ='1')then
					Instruction<=Din(11 downto 0);
				end if;
		end process;
end struct;