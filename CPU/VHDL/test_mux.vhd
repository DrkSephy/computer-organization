-- test Multiplexer

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_mux is
end test_mux;

Architecture arch_test of test_mux is

Component multiplexer port(
	sel				:in std_logic_vector(3 downto 0);
	reg0			:in std_logic_vector(15 downto 0);
	reg1			:in std_logic_vector(15 downto 0);
	reg2			:in std_logic_vector(15 downto 0);
	reg3			:in std_logic_vector(15 downto 0);
	reg4			:in std_logic_vector(15 downto 0);
	reg5			:in std_logic_vector(15 downto 0);
	reg6			:in std_logic_vector(15 downto 0);
	reg7			:in std_logic_vector(15 downto 0);
	Din				:in std_logic_vector(15 downto 0);
	regG			:in std_logic_vector(15 downto 0);
	bus_line		:out std_logic_vector(15 downto 0)
	);
end component;

signal	sel	: std_logic_vector(3 downto 0);
signal	reg0 :std_logic_vector(15 downto 0):= "0000000000000000";
signal	reg1 :std_logic_vector(15 downto 0):= "0000000000000001";
signal	reg2 :std_logic_vector(15 downto 0):= "0000000000000010";
signal	reg3 :std_logic_vector(15 downto 0):= "0000000000000011";
signal	reg4 :std_logic_vector(15 downto 0):= "0000000000000100";
signal	reg5 :std_logic_vector(15 downto 0):= "0000000000000101";
signal	reg6 :std_logic_vector(15 downto 0):= "0000000000000110";
signal	reg7 :std_logic_vector(15 downto 0):= "0000000000000111";
signal	Din :std_logic_vector(15 downto 0):=  "1111111111111111";
signal	regG :std_logic_vector(15 downto 0):= "1010101010101010";
signal	bus_line :std_logic_vector(15 downto 0);
signal	expected :std_logic_vector(15 downto 0);
signal error: std_logic:= '0';

begin

mux: multiplexer port map
(
	sel => sel,
	reg0 => reg0,
	reg1 => reg1,
	reg2 => reg2,
	reg3 => reg3,
	reg4 => reg4,
	reg5 => reg5,
	reg6 => reg6,
	reg7 => reg7,
	Din => Din,
	regG => regG,
	bus_line => bus_line
);

process begin

for k in 0 to 7 loop
	wait for 50 ns;
	sel <= std_logic_vector(to_unsigned(k, sel'length));
	expected <= std_logic_vector(to_unsigned(k, expected'length));
	wait for 50 ns;
	if (expected /= bus_line) then
		error <= '1';
	end if;
end loop;

-- test Din
	wait for 50 ns;
	sel <= "1000";
	wait for 50 ns;
	if (bus_line /= "1111111111111111") then
		error <= '1';
	end if;
	wait for 50 ns;

-- test regG
	wait for 50 ns;
	sel <= "1001";
	wait for 50 ns;
	if (bus_line /= "1010101010101010") then
		error <= '1';
	end if;
	wait for 50 ns;

	-- report test results
	if (error = '0') then
		report "No errors detected. Simulation successful" severity failure;
	else
		report "Error detected" severity failure;
	end if;
	
end process;
end arch_test;