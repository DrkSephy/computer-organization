--general 16-bit register

library ieee;
use ieee.std_logic_1164.all;

entity Reg16 is
port(
	clock			:in std_logic;
	enable			:in std_logic;
	D				:in std_logic_vector(15 downto 0);
	Q				:out std_logic_vector(15 downto 0)
	);
End Reg16;

architecture struct of Reg16 is
	Begin
		Process(Clock)
			Begin
				if(rising_edge(clock) and enable ='1')then
					Q <= D;
				end if;
		end process;
end struct;