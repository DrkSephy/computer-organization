--Multiplexer for CPU

library ieee;
use ieee.std_logic_1164.all;

entity multiplexer is
	port(
	sel				:in std_logic_vector(3 downto 0);
	reg0_in			:in std_logic_vector(15 downto 0);
	reg1_in			:in std_logic_vector(15 downto 0);
	reg2_in			:in std_logic_vector(15 downto 0);
	reg3_in			:in std_logic_vector(15 downto 0);
	reg4_in			:in std_logic_vector(15 downto 0);
	reg5_in			:in std_logic_vector(15 downto 0);
	reg6_in			:in std_logic_vector(15 downto 0);
	reg7_in			:in std_logic_vector(15 downto 0);
	Din				:in std_logic_vector(15 downto 0);
	regG_in			:in std_logic_vector(15 downto 0);
	bus_line		:out std_logic_vector(15 downto 0)
	);
end multiplexer;

architecture struct of multiplexer is
	begin
		process(sel)
			begin
				if(sel = "0000")then
					bus_line <= reg0_in;
				elsif(sel = "0001")then
					bus_line <= reg1_in;
				elsif(sel = "0010")then
					bus_line <= reg2_in;
				elsif(sel = "0011")then
					bus_line <= reg3_in;
				elsif(sel = "0100")then
					bus_line <= reg4_in;
				elsif(sel = "0101")then
					bus_line <= reg5_in;
				elsif(sel = "0110")then
					bus_line <= reg6_in;
				elsif(sel = "0111")then
					bus_line <= reg7_in;
				elsif(sel = "1000")then
					bus_line <= Din;
				elsif(sel = "1001")then
					bus_line <= regG_in;
				end if;
		end process;
end struct;	
				