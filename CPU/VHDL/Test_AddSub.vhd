--Test script for Adder/Subtracter

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_AddSub is
end test_AddSub;

architecture arch_test of test_AddSub is
component adder
	port(
		sign	     	 :in std_logic;
		Rx			      :in std_logic_vector(15 downto 0):= "0000000000000000";
		Ry 			      :in std_logic_vector(15 downto 0):= "0000000000000000";
		Result		    :out std_logic_vector(15 downto 0):="0000000000000000"
	);
end component;

signal sign			  	:std_logic;
signal RX		    	:std_logic_vector(15 downto 0):="0000000000000000";
signal RY			    :std_logic_vector(15 downto 0):="0000000000000000";
signal result		 	:std_logic_vector(15 downto 0):="0000000000000000";	
signal Error		  	:std_logic:='0';
signal clk	    		:std_logic:='1';

begin

Addsub: adder port map
(
	sign => sign,
	Rx 	 => RX,
	Ry	 => RY,
	Result => result
);

process
	begin
		sign <= '1';
		wait for 5 ns;
		clk <= not clk;
		Rx <= "0000000011110000";
		wait for 5 ns;
		clk <= not clk;
		Ry <= "0000000000001111";
		wait for 5 ns;
		clk <= not clk;
		if(result /="0000000011111111")then
			error <= '1';
		end if;
		
		sign <= '1';
		wait for 5 ns;
		clk <= not clk;
		Rx <= "1111111100000000";
		wait for 5 ns;
		clk <= not clk;
		Ry <= "0000111100000000";
		wait for 5 ns;
		clk <= not clk;
		if(result /="1111000000000000")then
			error <= '1';
		end if;
		
		if(error='0) then
			report "Test failed. Errors detected.";
		else
			report"Test successful. No errors detected." severity failure;
		end if;
	end process;
end arch_test;