-- Basic Control Unit

library ieee;
use ieee.std_logic_1164.all;

entity control_unit is
	port(
	run, reset, clock : in std_logic;
	sel				:out std_logic_vector(3 downto 0);
	done, regA_en, regG_en, IR_en, minus_sign :out std_logic;
	reg0_en, reg1_en, reg2_en, reg3_en, reg4_en, reg5_en, reg6_en, reg7_en : out std_logic;
	instruction	:in std_logic_vector(11 downto 0)
	);
end control_unit;

architecture behavior of control_unit is

type state_type is(
	idle, opcode, adding_subtracting, store_result
);

signal current_state, next_state: state_type;
signal IR, XXXX, YYYY : std_logic_vector(3 downto 0);
constant mov : std_logic_vector(3 downto 0) := "0000";
constant movi : std_logic_vector(3 downto 0) := "0001";
constant add : std_logic_vector(3 downto 0) := "0010";
constant sub : std_logic_vector(3 downto 0) := "0011";

begin -- architecture

IR <= instruction(11 downto 8);
XXXX <= instruction(7 downto 4);
YYYY <= instruction (3 downto 0);

state_process: process (next_state) begin
	current_state <= next_state;
end process;

clock_process: process(clock, run, reset) begin
	if( run = '1' and reset = '0'  ) then
		if(rising_edge(clock)) then
			case current_state is
				when idle =>
					-- disable all registers
					reg0_en <= '0';
					reg1_en <= '0';
					reg2_en <= '0';
					reg3_en <= '0';
					reg4_en <= '0';
					reg5_en <= '0';
					reg6_en <= '0';
					reg7_en <= '0';
					regA_en <= '0';
					regG_en <= '0';				
					-- Fetch Instruction
					IR_en <= '1';	
					-- update state
					done <= '0';
					next_state <= opcode;
					
				when opcode =>
					IR_en <= '0';
					if (IR = mov) then -- MOV RX, RY
						sel <= YYYY;
						-- Enable RegXXXX
						case XXXX is
							when "0000" => reg0_en <= '1';
							when "0001" => reg1_en <= '1';
							when "0010" => reg2_en <= '1';
							when "0011" => reg3_en <= '1';
							when "0100" => reg4_en <= '1';
							when "0101" => reg5_en <= '1';
							when "0110" => reg6_en <= '1';
							when "0111" => reg7_en <= '1';
							when others => null;-- do nothing, perhaps error flag?
						end case;				
						-- update state
						done <= '1';
						next_state <= idle;
					elsif(IR = movi) then -- MOVI RX, _VALUE
						-- Enable D_IN
						sel <= "1000";
						-- Enable RegXXXX
						case XXXX is
							when "0000" => reg0_en <= '1';
							when "0001" => reg1_en <= '1';
							when "0010" => reg2_en <= '1';
							when "0011" => reg3_en <= '1';
							when "0100" => reg4_en <= '1';
							when "0101" => reg5_en <= '1';
							when "0110" => reg6_en <= '1';
							when "0111" => reg7_en <= '1';
							when others => null; -- do nothing, perhaps error flag?
						end case;		
						-- update state
						done <= '1';
						next_state <= idle;
					elsif(IR = add or IR = sub) then -- Add/Sub
						sel <= XXXX;
						regA_en <= '1';	
						-- update state
						next_state <= adding_subtracting;
					else
						next_state <= idle; -- if wrong opcode
					end if;
					
				when adding_subtracting =>
					sel <= YYYY;
					regA_en <= '0';
					regG_en <= '1';
					
					if (IR = add) then
						minus_sign <= '0';
					elsif(IR = sub) then
						minus_sign <= '1';
					end if;
					
					-- update state
					done <= '0';
					next_state <= store_result;
					
				when store_result =>
					-- Enable RegG
					sel <= "1001";
					regG_en <= '0';
					-- Enable RegXXXX
					case XXXX is
							when "0000" => reg0_en <= '1';
							when "0001" => reg1_en <= '1';
							when "0010" => reg2_en <= '1';
							when "0011" => reg3_en <= '1';
							when "0100" => reg4_en <= '1';
							when "0101" => reg5_en <= '1';
							when "0110" => reg6_en <= '1';
							when "0111" => reg7_en <= '1';
							when others => null; -- do nothing, perhaps error flag?
					end case;
					
					-- update state
					done <= '1';
					next_state <= idle;
			end case;
		end if;
	elsif( run = '0') then
		IR_en <= '0';
		next_state <= idle;
	else -- reset = 1 or run = 0
		IR_en <= '1';
		next_state <= idle;
	end if;
end process;

end behavior;