library ieee;
use ieee.std_logic_1164.all;

entity test_Reg16 is
end test_Reg16;

architecture arch_test of test_Reg16 is
component Reg16
	Port(
	clock			:in std_logic;
	enable			:in std_logic;
	D				:in std_logic_vector(15 downto 0);
	Q				:out std_logic_vector(15 downto 0)
	);
end component;

signal clk		  : std_logic:='1';
signal en		  : std_logic;
signal Input	  : std_logic_vector(15 downto 0):="0000000000000000";
Signal Output	  : std_logic_vector(15 downto 0):="0000000000000000";
signal Error  	  : std_logic:='0';
  
begin
	
Reg:Reg16 port map
(
	clock => clk,
	enable => en,
	D => Input,
	Q => Output
);

process
	begin
		en <= '1';
		clk <= not clk;
		Input <= "0000111100001111";
		clk <= not clk;
		Output <= "0000111100001111";
		clk <= not clk;
		
		wait for 10 ns;
		clk <= not clk;
		Input <= "1010101010101010";
		clk <= not clk;
		Output <= "1010101010101010";
		clk <= not clk;
		
		
		report"Test successful. No errors detected." severity failure;
	end process;
end arch_test;
		