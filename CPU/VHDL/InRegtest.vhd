--Instruction register test script

library ieee;
use ieee.std_logic_1164.all;

entity test_InReg is
end test_InReg;

architecture arch_test of test_InReg is
component InReg
	port(
	clock 		:in std_logic;
	enable		:in std_logic;
	Din 		:in std_logic_vector(15 downto 0):="0000000000000000";
	Instruction	:out std_logic_vector(11 downto 0):="000000000000"
	);
end component;

signal clk	:std_logic;
signal en	:std_logic;
signal Data	:std_logic_vector(15 downto 0):="0000000000000000";
signal op	:std_logic_vector(11 downto 0):="000000000000";

begin

Instr: InReg port map(
	clock => clk,
	enable => en,
	Din => Data,
	Instruction => op
	);
	
process
	begin
		en <= '1';
		wait for 5 ns;
		clk <= not clk;
		Data <= "0000000000000000";
		wait for 5 ns;
		clk <= not clk;
		op <= "000000000000";
		wait for 5 ns;
		clk <= not clk;
		
		Data <= "0001000000000000";
		wait for 5 ns;
		clk <= not clk;
		op <= "000100000000";
		wait for 5 ns;
		clk <= not clk;
		
		report"Test successful. No errors detected." severity failure;
	end process;
end arch_test;