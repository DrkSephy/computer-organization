--Control unit 2

library ieee;
use ieee.std_logic.1164.all;
use ieee.numeric_std.all;

entity control_unit is
	port(
		run			:in std_logic;
		reset 		:in std_logic;
		clock		:in std_logic;
		IRin		:in std_logic_vector(8 downto 0);
		Done		:out std_logic;
		IRen		:out std_logic;
		Mux			:out std_logic_vector(3 downto 0);
		Reg0		:out std_logic;
		Reg1		:out std_logic;
		RegA		:out std_logic;
		RegG		:out std_logic;
		AddSub		:out std_logic;
		);
end control_unit;

architecture struct of control_unit is
	type state_type is(opcode, idle, add1, add2);
	signal current_state:state_type;
	signal next_state	:state_type;
	signal clk			:std_logic;
	
begin
	process(clock,reset)
		begin
			if(reset='0')then
				current_state <= opcode;
			elsif(rising_edge(clock))then
				current_state <= next_state;
				clk <= not clk;
			end if;
	end process;
	
	process(clk)
		begin
			if(clock ='1')then
				case current_state is
					when opcode =>
						Reg0 <= '0';
						Reg1 <= '0';
						Reg2 <= '0';
						Reg3 <= '0';
						Reg4 <= '0';
						Reg5 <= '0';
						Reg6 <= '0';
						Reg7 <= '0';
						if(IRin(11 downto 8) = "0000")then --opcode is 0x0
							Mux <= "0" & IRin(3 downto 0); --Register Ry
							Case IRin(7 downto 4) is --Register Rx
								when "0000" => Reg0 <= '1';
								when "0001" => Reg1 <= '1';
								when "0010" => Reg2 <= '1';
								when "0011" => Reg3 <= '1';
								when "0100" => Reg4 <= '1';
								when "0101" => Reg5 <= '1';
								when "0110" => Reg6 <= '1';
								when "0111" => Reg7 <= '1';
								when others => null;
							end case;
							next_state <= idle;
							--not done yet, we need to go to our move state
							--in our state diagram, idle is also our move state
							Done <= '0';
							IRen <= '0'; --we still need to do the move
						elsif(IRin(11 downto 8) ="001")then --movi Rx, constant
							mux <= "1000"; --Din will move constant in
							case IRin(7 downto 4) is --Register Rx
								when "0000" => Reg0 <= '1';
								when "0001" => Reg1 <= '1';
								when "0010" => Reg2 <= '1';
								when "0011" => Reg3 <= '1';
								when "0100" => Reg4 <= '1';
								when "0101" => Reg5 <= '1';
								when "0110" => Reg6 <= '1';
								when "0111" => Reg7 <= '1';
								when others => null;
							end case;
							next_state <= idle;
							Done <= '0';
							IRen <= '0';
						--add or subtract
						elsif(IRin(11 downto 8) = "0010" or IRin(11 downto 8) = "0011")then
							Mux <= "0" & IRin(7 downto 4) --Register Rx
								RegA <= '1'; --Put Rx in A on the next clock tick
								RegG <= '1'; --Enable temp Register G to hold result
								next_state <= add1;
								Done <= '0';
								IRen <= '0';
						end if;
						when idle =>
							next_state <= opcode;
							Reg0 <= '0';
							Reg1 <= '0';
							Reg2 <= '0';
							Reg3 <= '0';
							Reg4 <= '0';
							Reg5 <= '0';
							Reg6 <= '0';
							Reg7 <= '0';
							--Get new instruction on next clock cycle
							IRen <= '1'; 
							Done <= '1';
						when add1 => 
							RegA <= '0';
							--Put Register Ry on bus on next clock cycle
							Mux <= '0' & IRin(3 downto 0); 
							AddSub <= IRin(11);
							next_state <= add2;
						when add2 =>
							Case IRin(7 downto 4) is --register Rx
								when "0000" => Reg0 <= '1';
								when "0001" => Reg1 <= '1';
								when "0010" => Reg2 <= '1';
								when "0011" => Reg3 <= '1';
								when "0100" => Reg4 <= '1';
								when "0101" => Reg5 <= '1';
								when "0110" => Reg6 <= '1';
								when "0111" => Reg7 <= '1';
								when others => null;
							end case;
							RegG <= '0';
							--Put result of Adder on bus
							Mux <= "1001";
							--get new instruction
							next_state <= opcode;
							Done <= '1';
							IRen <= '1';
						when others => null;
					End case;
				end if;
			end process;
		end struct;