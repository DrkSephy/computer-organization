--CPU Display

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity CPUDisplay is
	port(
		Run		:in std_logic:='1';
		Reset	:in std_logic:='1';
		Clock	:in std_logic;
		Din		:in std_logic_vector(15 downto 0);
		Done	:out std_logic;
		BusLine	:out std_logic_vector(15 downto 0);
		Display	:out std_logic_vector(55 downto 0)
		);
end CPUDisplay;

architecture arch of CPUDisplay is
component CPU
	port(
		Run		:in std_logic:='1';
		Reset	:in std_logic:='1';
		Clock	:in std_logic;
		Din		:in std_logic_vector(15 downto 0);
		Done	:out std_logic;
		BusLine	:out std_logic_vector(15 downto 0);
		Reg0Out	:out std_logic_vector(15 downto 0);
		Reg1Out	:out std_logic_vector(15 downto 0);
		Reg2Out	:out std_logic_vector(15 downto 0);
		Reg3Out	:out std_logic_vector(15 downto 0);
		Reg4Out	:out std_logic_vector(15 downto 0);
		Reg5Out	:out std_logic_vector(15 downto 0);
		Reg6Out	:out std_logic_vector(15 downto 0);
		Reg7Out	:out std_logic_vector(15 downto 0)
		);
end component;

component Hex_to_Seven
	port(
		en		:in std_logic;
		input	:in std_logic_vector(31 downto 0);
		hex0	:out std_logic_vector(6 downto 0);
		hex1	:out std_logic_vector(6 downto 0);
		hex2	:out std_logic_vector(6 downto 0);
		hex3	:out std_logic_vector(6 downto 0);
		hex4	:out std_logic_vector(6 downto 0);
		hex5	:out std_logic_vector(6 downto 0);
		hex6	:out std_logic_vector(6 downto 0);
		hex7	:out std_logic_vector(6 downto 0)
		);
end component;

--Only need to wire up registers 0 and 1
signal reg0Hex	:std_logic_vector(15 downto 0):="0000000000000000";
signal reg1Hex	:std_logic_vector(15 downto 0):="0000000000000000";
signal hexInput	:std_logic_vector(31 downto 0);
signal msb		:std_logic_vector(15 downto 0); --Instruction register


begin
HexDisplay: CPU port map(
	Run => run,
	Reset => reset,
	Clock => clock,
	Din => Din,
	BusLine => lsb,
	Done => Done,
	reg0out => reg0Hex,
	reg1out=> reg1Hex
);

Hex	:Hex_to_Seven port map(
	en => '1',
	input => HexInput,
	Hex0 => display(6 downto 0),
	Hex1 => display(13 downto 7),
	Hex2 => display(20 downto 14),
	Hex3 => display(27 downto 21),
	Hex4 => display(34 downto 28),
	Hex5 => display(41 downto 35),
	Hex6 => display(48 downto 42),
	Hex7 => display(55 downto 49)
);

--we only need to display the 8 lsb of reg 0 and 1
process(clock, reset)
	begin
		if(reset ='0')then
			HexInput <=(others =>'0');
		elsif(rising_edge(clock))then
		--Put contents of register 0 and register 1 on display
			HexInput <= reg0Hex(7 downto 0) & reg1hex(7 downto 0); 
		--put contents of Instruction register on display
			Busline <= msb;
		end if;
	end process;
end arch;
		

