--Adder/Subtractor

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity adder is
	port(
		minus_sign		: in std_logic;
		Rx				: in std_logic_vector(15 downto 0);
		Ry				: in std_logic_vector(15 downto 0);
		result			: out std_logic_vector(15 downto 0)
	);
end adder;

architecture struct of adder is
	begin
		process(minus_sign,Rx,Ry)
			begin
				if(minus_sign ='0')then
					result <= std_logic_vector(unsigned(Rx) + unsigned(Ry));
				else
					result <= std_logic_vector(unsigned(Rx) - unsigned(Ry));
				end if;
		end process;
end struct;