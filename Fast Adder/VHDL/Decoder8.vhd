library ieee;
use ieee.std_logic_1164.all;

entity decoder8 is
	Port(
		I	: in std_logic_vector(2 downto 0);
		O 	: out std_logic_vector(7 downto 0)
	);
end decoder8;

architecture struct of decoder8 is
	Begin
		Process (I)
			Begin
				If(I = "000") then
					O <= "00000001";
				elsif(I = "001") then
					O <= "00000010";
				elsif(I = "010") then
					O <= "00000100";
				elsif(I = "011") then
					O <= "00001000";
				elsif(I = "100") then
					O <= "00010000";
				elsif(I = "101") then
					O <= "00100000";
				elsif(I = "110") then
					O <= "01000000";
				elsif(I = "111") then
					O <= "10000000";
				end if;
		end process;
end struct;
				
				