library ieee;
use ieee.std_logic_1164.all;

entity reg8 is
	port(
		D	: in std_logic_vector(7 downto 0);
		clock : in std_logic;
		enable : in std_logic;
		Q	: out std_logic_vector(7 downto 0)
		);
end reg8;

architecture struct of reg8 is
	begin
		process(clock)
			begin
				if(rising_edge(clock) and enable='1') then
					Q <= D;
				end if;
		end process;
end struct;