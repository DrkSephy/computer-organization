library ieee;
use ieee.std_logic_1164.all;

entity multiplexor8 is
	port(
		I0, I1, I2, I3, I4, I5, I6, I7 	: in std_logic_vector(7 downto 0);
		S								: in std_logic_vector(2 downto 0);
		O								: out std_logic_vector(7 downto 0)
	);
end multiplexor8;

architecture behav of multiplexor8 is
	begin
		process(S, I0, I1, I2, I3, I4, I5, I6, I7)
			begin
				If(S = "000") then
					O <= I0;
				elsif(S = "001") then
					O <= I1;
				elsif(S = "010") then
					O <= I2;
				elsif(S = "011") then
					O <= I3;
				elsif(S = "100") then
					O <= I4;
				elsif(S = "101") then
					O <= I5;
				elsif(S = "110") then
					O <= I6;
				elsif(S = "111") then
					O <= I7;
				end if;
		end process;
	end behav;