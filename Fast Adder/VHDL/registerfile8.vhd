library ieee;
use ieee.std_logic_1164.all;

entity registerfile8 is
port 
	( 
	clock8: in std_logic;
	W_Addr: in std_logic_vector(2 downto 0);
	DataIn: in std_logic_vector(7 downto 0);
	R_Addr_A: in std_logic_vector(2 downto 0);
	DataOutA: out std_logic_vector(7 downto 0)
);
end registerfile8;

architecture struct of registerfile8 is
	component Reg8
	port ( 
	clock		: in std_logic;
	D			: in std_logic_vector(7 downto 0);
	Enable		: in std_logic;
	Q			: out std_logic_vector(7 downto 0)
);
end component;

component decoder8
	port (
	I			: in std_logic_vector(2 downto 0);
	O			: out std_logic_vector(7 downto 0)
	);
end component;

component multiplexor8
	port ( 
	I7			: in std_logic_vector(7 downto 0);
	I6			: in std_logic_vector(7 downto 0);
	I5			: in std_logic_vector(7 downto 0);
	I4			: in std_logic_vector(7 downto 0);
	I3			: in std_logic_vector(7 downto 0);
	I2			: in std_logic_vector(7 downto 0);
	I1			: in std_logic_vector(7 downto 0);
	I0			: in std_logic_vector(7 downto 0);
	S			: in std_logic_vector(2 downto 0);
	O			: out std_logic_vector(7 downto 0)
);
end component;

signal ss1: std_logic;
signal ss2: std_logic;
signal ss3: std_logic;
signal ss4: std_logic;
signal ss5: std_logic;
signal ss6: std_logic;
signal ss7: std_logic;
signal ss8: std_logic;

signal ss: std_logic_vector(7 downto 0);
signal DataOut1:  std_logic_vector(7 downto 0);
signal DataOut2:  std_logic_vector(7 downto 0);
signal DataOut3:  std_logic_vector(7 downto 0);
signal DataOut4:  std_logic_vector(7 downto 0);
signal DataOut5:  std_logic_vector(7 downto 0);
signal DataOut6:  std_logic_vector(7 downto 0);
signal DataOut7:  std_logic_vector(7 downto 0);
signal DataOut8:  std_logic_vector(7 downto 0);

begin
decoder1: decoder8 port map(
	I => W_Addr, O => ss);
	

ss1 <= ss(0) and (clock8);
ss2 <= ss(1) and (clock8);
ss3 <= ss(2) and (clock8);
ss4 <= ss(3) and (clock8);
ss5 <= ss(4) and (clock8);
ss6 <= ss(5) and (clock8);
ss7 <= ss(6) and (clock8);
ss8 <= ss(7) and (clock8);

R1: Reg8 port map(
	Clock => ss1,
	D => DataIn,
	Q => DataOut1,
	Enable => ss(0)
);

R2 : Reg8 port map(
	Clock => ss2,
	D => DataIn,
	Q => DataOut2,
	Enable => ss(1)
);

R3: Reg8 port map(
	Clock => ss3,
	D => DataIn,
	Q => DataOut3,
	Enable => ss(2)
);

R4: Reg8 port map(
	Clock => ss4,
	D => DataIn,
	Q => DataOut4,
	Enable => ss(3)
);

R5 : Reg8 port map(
	Clock => ss5,
	D => DataIn,
	Q => DataOut5,
	Enable => ss(4)
);

R6 : Reg8 port map(
	Clock => ss6,
	D => DataIn,
	Q => DataOut6,
	Enable => ss(5)
);

R7 : Reg8 port map(
	Clock => ss7,
	D => DataIn,
	Q => DataOut7,
	Enable => ss(6)
);

R8 : Reg8 port map(
	Clock => ss8,
	D => DataIn,
	Q => DataOut8,
	Enable => ss(7)
);

mux1 : multiplexor8 port map(
	I0 => DataOut1,
	I1 => DataOut2,
	I2 => DataOut3,
	I3 => DataOut4,
	I4 => DataOut5,
	I5 => DataOut6,
	I6 => DataOut7,
	I7 => DataOut8,
	S => R_Addr_A,
	O => DataOutA
);


end struct;
