library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Adder is
port (
  Clock_50_MHZ :in std_logic;
  En :in std_logic;
  Data_Registerfile : in std_logic_vector(7 downto 0);
  Read_Address: out std_logic_vector(2 downto 0);
  Output : out std_logic_vector(11 downto 0)
);
end Adder;

architecture behavior of Adder is

TYPE State_type IS (
	Idle, IncrementAddr, readData, Output
);

SIGNAL currentState, nextState : State_type;

signal r_address : std_logic_vector(2 downto 0) := "000";
signal data:  std_logic_vector(7 downto 0);
signal sum : unsigned(11 downto 0);

begin --begin the architecture

state_process: process (nextState)
begin
    currentState <= nextState;
end process;

clock_process: process (Clock_50_MHZ, En)
begin
       if (En = '1') then
               if (rising_edge(clock_50_mhz)) then
                       case currentState is
                        when Idle =>
									-- Reset all values
								 	Read_Address <= r_address;
									sum <= "000000000000";
                                    
									-- Update state
									nextState <= readData;
								when IncrementAddr =>
									-- Update sum
									sum <= sum + unsigned(data);
									Read_Address <= r_address;
									
									-- Update state
									if (r_address = "111") then -- if we reached 8 numbers we are done
										nextState <= Output;
									else -- not done quite yet!
										nextState <= readData;
									end if;
                        when readData =>
                           -- fetch data
									data <= Data_Registerfile;
									   
									-- increment read address
									r_address <= std_logic_vector(unsigned(r_address) + 1);
									   
									-- jump to next state
                           nextState <= IncrementAddr;
								when Output => 
									-- Update output
									Output <= std_logic_vector(sum);
									
									-- Update state
									nextState <= Idle;
                       end case;
               end if;
       else --if the circuit is not enabled
            nextState <= Idle; -- Idle will reset the Adder
				Output <= "000000000000";
				r_address <= "000";
       end if;
end process;

end Behavior;