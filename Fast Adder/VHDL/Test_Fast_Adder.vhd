library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity test_fast_adder is
end test_fast_adder;

architecture arch of test_fast_adder is
component Fast_Adder
port(
	Clock_50_MHZ :in std_logic;
	En :in std_logic;
	Clock_User :in std_logic;
	Data_User : in std_logic_vector(7 downto 0);
	Address_User: in std_logic_vector(2 downto 0);
	Sev_Seg_Output_1,Sev_Seg_Output_2,Sev_Seg_Output_3 : out std_logic_vector(3 downto 0);
end component;

signal Data_User : std_logic_vector(7 downto 0);
signal Address_User: std_logic_vector(2 downto 0);
signal Sev_1, Sev_2, Sev_3 : std_logic_vector(3 downto 0);
signal En, Clk : std_logic;
signal error : std_logic := '0';
signal Clock_50_MHZ: std_logic;
 
begin
FastAdder: Fast_Adder port map(
		Data_User => Data_User,
		En => En, 
		Clock_User => Clk,
		Clock_50_MHZ => Clock_50_MHZ,
		Sev_Seg_Output_1 => Sev_1,
		Sev_Seg_Output_2 => Sev_2,
		Sev_Seg_Output_3 => Sev_3);

		
process begin
		Clk <= '0';
		Clock_50_MHZ <= '1';
		En <= '0';
		Data_User(0) <= "00000000";
		Data_User(1) <= "00000000";
		Data_User(2) <= "00000000";
		Data_User(3) <= "00000000";
		Data_User(4) <= "00000000";
		Data_User(5) <= "00000000";
		Data_User(6) <= "00000000";
		Data_User(7) <= "00000000";
		wait for 100 ns;
		En <= '1';
		if(Sev_1 /= "0000") then
			error <= '1';
		elsif(Sev_2 /= "0000") then
			error <= '1';
		elsif(Sev_3 /= "0000") then
			error <= '1';
		else 
		  error <= '0';
		end if
		wait for 25 ns;
		
		Clk <= '0';
		Clock_50_MHZ <= '1';
		En <= '0';
		Data_User(0) <= "00001111";
		Data_User(1) <= "11110000";
		Data_User(2) <= "11111111";
		Data_User(3) <= "00000000";
		Data_User(4) <= "00000000";
		Data_User(5) <= "00000000";
		Data_User(6) <= "00000000";
		Data_User(7) <= "00000000";
		wait for 100 ns;
		En <= '1';
		if(Sev_1 /= "1111") then
			error <= '1';
		elsif(Sev_2 /= "1111") then
			error <= '1';
		elsif(Sev_3 /= "0000") then
			error <= '1';
		else
		  error <= '0';
		end if
		wait for 25 ns;
		
		Clk <= '0';
		Clock_50_MHZ <= '1';
		En <= '0';
		Data_User(0) <= "00000001";
		Data_User(1) <= "00000010";
		Data_User(2) <= "00000100";
		Data_User(3) <= "00001000";
		Data_User(4) <= "00010000";
		Data_User(5) <= "00100000";
		Data_User(6) <= "01000000";
		Data_User(7) <= "10000000";
		wait for 100 ns;
		En <= '1';
		if(Sev_1 /= "1110") then
			error <= '1';
		elsif(Sev_2 /= "1111") then
			error <= '1';
		elsif(Sev_3 /= "0001") then
			error <= '1';
		else
		   error <= '0';
		end if
		wait for 25 ns;
		
		if(error = '0') then
			report "No errors detected. Simulation was successful!" severity failure;
		else
			report "Error detected" severity failure;
		end if;
	end process;
end arch;
		