library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Fast_Adder is
port (
	Clock_50_MHZ :in std_logic;
	En :in std_logic;
	Clock_User :in std_logic;
	Data_User : in std_logic_vector(7 downto 0);
	Address_User: in std_logic_vector(2 downto 0);
	Sev_Seg_Output_1,Sev_Seg_Output_2,Sev_Seg_Output_3 : out std_logic_vector(3 downto 0)
);
end Fast_Adder;

architecture arch of Fast_Adder is
component Adder
port (
	Clock_50_MHZ :in std_logic;
	En :in std_logic;
	Data_Registerfile : in std_logic_vector(7 downto 0);
	Read_Address: out std_logic_vector(2 downto 0);
	Output : out std_logic_vector(11 downto 0)
);
end component;

component registerfile_8_by_8
port ( 
	clock4: in std_logic;
	W_Addr: in std_logic_vector(2 downto 0);
	DataIn: in std_logic_vector(7 downto 0);
	R_Addr: in std_logic_vector(2 downto 0);
	DataOut: out std_logic_vector(7 downto 0)
);
end component;

signal Data: std_logic_vector(7 downto 0);
signal RAddr : std_logic_vector(2 downto 0);
signal Output : std_logic_vector(11 downto 0);
begin
Sev_Seg_Output_1 <=  Output(11 Downto 8);
Sev_Seg_Output_2 <=  Output(7 Downto 4);
Sev_Seg_Output_3 <=  Output(3 Downto 0);
A: Adder port map
(
Clock_50_MHZ => Clock_50_MHZ,
En => En,
Data_Registerfile => Data,
Read_Address => RAddr,
Output => Output
);
RF: registerfile_8_by_8 port map(
clock4 => Clock_User,
W_Addr => Address_User,
DataIn => Data_User,
R_Addr => RAddr,
DataOut => Data
);
end arch;

