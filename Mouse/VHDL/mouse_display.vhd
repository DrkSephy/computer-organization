--Mouse Display

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity mouse_display is
port(
	clk 					:in std_logic;
	reset					:in std_logic;
	ps2_data			 	:inout std_logic;
	ps2_clk 				:inout std_logic;
	display					:out std_logic_vector(55 downto 0)
	);

end mouse_display;

--define components from mouse interface
architecture arch of mouse_display is
component mouse_interface
	port(
		clk, reset			:in std_logic;
		ps2_data			:inout std_logic;
		ps2_clk 			:inout std_logic;
		m_done 				:out std_logic;
		x, y 				:out std_logic_vector(8 downto 0);
		btn  				:out std_logic_vector(2 downto 0)
	);
end component;

--we need the hex -> seven display, taken from keyboard lab
component hex_to_seven
	port(
		en					:in std_logic;
		input				:in std_logic_vector(31 downto 0);
		HEX0				:out std_logic_vector(6 downto 0);
		HEX1				:out std_logic_vector(6 downto 0);
		HEX2				:out std_logic_vector(6 downto 0);
		HEX3				:out std_logic_vector(6 downto 0);
		HEX4				:out std_logic_vector(6 downto 0);
		HEX5				:out std_logic_vector(6 downto 0);
		HEX6				:out std_logic_vector(6 downto 0);
		HEX7				:out std_logic_vector(6 downto 0)
	);
end component;

--need signals here for the port mapping

signal xy 	   : std_logic_vector(31 downto 0);
signal x, y    : std_logic_vector(8 downto 0);
signal bt_n    : std_logic_vector(2 downto 0);
signal mdone   : std_logic;
signal current_X, next_X: unsigned(7 downto 0) := "00000000";
signal current_Y, next_Y: unsigned(7 downto 0) := "00000000";
signal NotReset: std_logic;

begin
	NotReset <= not reset;

mouse: mouse_interface port map(
	clk => Clk,
	reset => NotReset,
	ps2_data => ps2_Data,
	ps2_clk => ps2_clk,
	x => x,
	y => y,
	btn => bt_n,
	m_done => mdone
);

hex: Hex_to_Seven port map(
	en => '1',
	input => xy,
	HEX0 => display(6 downto 0),
	HEX1 => display(13 downto 7),
	HEX2 => display(20 downto 14),
	HEX3 => display(27 downto 21),
	HEX4 => display(34 downto 28),
	HEX5 => display(41 downto 35),
	HEX6 => display(48 downto 42),
	HEX7 => display(55 downto 49)
);

process(clk, reset)
	begin
		if(reset ='0') then
			current_X <= (others => '0');
			current_Y <= (others => '0');
		elsif(rising_edge(clk)) then
			current_X <= next_X;
			current_Y <= next_Y;
		end if;
end process;

	next_X <= current_X when mdone ='0'
	else current_X + unsigned(x(8 downto 1));
	next_Y <= current_Y when mdone ='0'
	else current_Y + unsigned(y(8 downto 1));
	
	xy <= "0000000000000000
	00000000" & std_logic_vector(current_X) & std_logic_vector(current_Y);

end arch;
