library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 
entity ps2_rt is
	port (
		Clk ,Reset: in std_logic; 
		ps2_Data : inout std_logic;
		ps2_Clk : inout std_logic; 
		write_en : std_logic;
		dout : out std_logic_vector( 7 downto 0);
		rec_done, tran_done : out std_logic; 
		din: in std_logic_vector( 7 downto 0)
		);
end ps2_rt;

architecture arch of ps2_rt is

component ps2_tran port
(
	Clk , Reset: in std_logic; 
	ps2_Data : inout std_logic;
	ps2_Clk : inout std_logic; 
	en : std_logic;
	tran_done: out std_logic; 
	tran_idle: out std_logic;
	din: in std_logic_vector( 7 downto 0)
);
end component;

component ps2_rec port
(
	Clk ,Reset: in std_logic;
	ps2Data : in std_logic; 
	ps2Clock : in std_logic; 
	rec_en : in std_logic; 
	rec_done: out std_logic; 
	Dout: out std_logic_vector( 7 downto 0)
);

end component;

--Enter your code here
signal tran_idle: std_logic;
begin
transmitter: ps2_tran port map
(
	Clk => Clk,
	Reset => Reset,
	en => write_en,
	din => din,
	ps2_Data => ps2_Data,
	ps2_clk => ps2_clk,
	tran_idle => tran_idle,
	tran_done => tran_done
);

reciever: ps2_rec port map
(
	clk => clk,
	reset => reset,
	ps2Data => ps2_Data,
	ps2Clock => ps2_Clk,
	rec_en => tran_idle,
	rec_done => rec_done,
	Dout => dout
);
end arch;