LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity test_Register_4_bit is
end test_Register_4_bit;

architecture test of test_Register_4_bit is 

component Fourbit_Register
Port (
	En, Clock   			: in std_logic;
	Input					: in std_logic_vector(3 Downto 0);
	Output 					: out std_logic_vector(3 Downto 0)
);
End component; 

signal Input	            : std_logic_vector(3 Downto 0);
signal En, Clk				: STD_LOGIC;
signal Output 				: std_logic_vector(3 Downto 0);
signal error       		    : std_logic := '0';

begin
testing: Fourbit_Register port map (Input=>Input,En=>En,Clock=>Clk,Output=>Output);
   
process
	begin
		
		Input <= "0000";
		Clk <= '0';
		EN <= '1';
		wait for 100 ns;
		Clk <= Not Clk;
		wait for 1 ns;
		if (Output /= "0000") then
			error <= '1';
		end if;
		

		wait for 100 ns;
		Clk <= Not Clk;
		EN <= '1';
		Input <= "1010";
		wait for 100 ns;
		Clk <= Not Clk;
		wait for 1 ns;
		if (Output /= "1010") then
			error <= '1';
		end if;	

		wait for 100 ns;
		Clk <= Not Clk;
		EN <= '0';
		Input <= "0000";
		wait for 100 ns;
		Clk <= Not Clk;
		wait for 1 ns;
		if (Output /= "1010") then
			error <= '1';
		end if;	

		wait for 100 ns;
		Clk <= Not Clk;
		EN <= '1';
		Input <= "0101";
		wait for 100 ns;
		Clk <= Not Clk;
		wait for 1 ns;
		if (Output /= "0101") then
			error <= '1';
		end if;	
		wait for 100 ns;
		
		if (error = '0') then
			report "No errors detected. Simulation successful" severity failure;
		else
			report "Error detected" severity failure;
		end if;	
		
	end process;
END test;
