--Control SR-Latch

Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ControlSR_Latch is
	port(
		Control, Set, Reset		: in STD_LOGIC;
		Q, notQ					         : inout STD_LOGIC);
end ControlSR_Latch;

architecture arch of ControlSR_Latch is
begin
		Q <= ((Set nand Control) nand notQ);
		notQ <= ((Reset nand Control) nand Q);
end arch;