--SR-Latch code
--Don't use inout logic
Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SR_Latch is
	port(
		Set, Reset	: in STD_LOGIC;
		Q, notQ		  : out STD_LOGIC);
end SR_Latch;


architecture arch of SR_Latch is
Signal tempQ, tempNotQ : STD_LOGIC;

begin
		Q <= tempQ;
		notQ <= tempNotQ;
		tempQ <= (Reset nor tempNotQ);
		tempNotQ <= Set nor tempQ;
		
end arch;