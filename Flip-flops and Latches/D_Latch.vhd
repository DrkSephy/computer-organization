--Code for D-Latch

Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity D_Latch is
	port(
		C, D			: in STD_LOGIC;
		Q, notQ			: inout STD_LOGIC
		);
end D_Latch;

architecture arch of D_Latch is
begin
		Q <= ((D nand C) nand notQ);
		notQ <= (((not D) nand C) nand Q);
end arch;
