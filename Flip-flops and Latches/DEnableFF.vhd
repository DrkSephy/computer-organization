library ieee;
use ieee.std_logic_1164.all;

entity DEnable_FF is
	port(
		Enable, D, DeClock	: in std_logic;
		Q, NotQ				: inout std_logic);
end DEnable_FF;

architecture arch of DEnable_FF is
	component Dflipflop 
	port(
		Input, Clock		: in std_logic;
		outQ, outnotQ		: inout std_logic);
	end component;
	
	signal E				: std_logic;
	
	begin
		process(Enable, D, DeClock)
			begin
				if (Enable = '1') then
					E <= D;
				elsif(Enable = '0') then
					E <= Q;
				end if;
			end process;
			
	Enableflipflop: Dflipflop port map(
		Input => E, Clock => DeClock, outQ => Q, outnotQ => NotQ);
end arch;