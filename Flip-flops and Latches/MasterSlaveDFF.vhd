Library ieee;
Use ieee.std_logic_1164.all;

entity Dflipflop is
	port(
		Input, Clock		: in std_logic;
		outQ, outnotQ		: inout std_logic);
end Dflipflop;

architecture arch of Dflipflop is
	component D_Latch
	port(
		C, D				: in std_logic;
		Q, notQ				: inout std_logic);
	end component;
	
	signal output, notClock : std_logic;
	
	begin
		notClock <= not Clock;
		MasterFF: D_Latch port map (C => notClock, D => Input, Q => output);
		SlaveFF: D_Latch port map(
		C => Clock, D => output, Q => outQ, NotQ => outNotQ);
end arch;
		