library ieee;
use ieee.std_logic_1164.all;

entity fourbitregister is
	port(
		En, Clock     : in std_logic;
		Input	        : in std_logic_vector(3 downto 0);
		Output	       : inout std_logic_vector(3 downto 0));
end fourbitregister;

architecture arch of Fourbit_Register is
	component DEnable_FF
		port(
			Enable, D, DeClock : in std_logic;
			Q, NotQ 		         : inout std_logic);
	end component;
	
	begin
		DFlipflop1: DEnable_FF port map
		(Enable => En, DeClock => Clock, D => Input(0), Q => Output(0));
		DFlipflop2: DEnable_FF port map
		(Enable => En, DeClock => Clock, D => Input(1), Q => Output(1));
		DFlipflop3: DEnable_FF port map
		(Enable => En, DeClock => Clock, D => Input(2), Q => Output(2));
		DFlipflop4: DEnable_FF port map
		(Enable => En, DeClock => Clock, D => Input(3), Q => Output(3));
end arch;
