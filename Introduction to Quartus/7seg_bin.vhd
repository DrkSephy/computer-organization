LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.all;
USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;
-- Binary to 7 Segment Decoder for LED Display
ENTITY dec_7seg_bin IS
PORT(
bin_digit : IN STD_LOGIC;
segment_a, segment_b, segment_c, segment_d, segment_e, segment_f,
segment_g : OUT std_logic);
END dec_7seg_bin;

ARCHITECTURE a OF dec_7seg_bin IS
SIGNAL segment_data : STD_LOGIC_VECTOR(6 DOWNTO 0);
BEGIN
PROCESS (bin_digit)
-- Binary to 7 Segment Decoder for LED Display
BEGIN -- Binary-digit is the 1 bit binary value to display
CASE bin_digit IS
WHEN '0' =>
segment_data <= "1111110";
WHEN '1' =>
segment_data <= "0110000";
END CASE;
END PROCESS;
-- extract segment data bits and invert
segment_a <= NOT segment_data(6);
segment_b <= NOT segment_data(5);
segment_c <= NOT segment_data(4);
segment_d <= NOT segment_data(3);
segment_e <= NOT segment_data(2);
segment_f <= NOT segment_data(1);
segment_g <= NOT segment_data(0);
END a;
