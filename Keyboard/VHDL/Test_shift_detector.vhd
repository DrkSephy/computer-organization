library ieee;
use ieee.std_logic_1164.all;

entity Test_shift_detector is
end Test_shift_detector;

Architecture arch of Test_shift_detector is

component shift_detector port
(
clock : in std_logic;
finished : in std_logic;
scan_code : in std_logic_vector(7 downto 0);
shift : out std_logic
);
end component;

Signal scan_code: std_logic_vector(7 downto 0);
Signal clock: std_logic := '0';
Signal finished, shift, error: std_logic;

begin

sd: shift_detector port map
(
	clock => clock,
  	finished => finished,
  	scan_code => scan_code,
  	shift => shift
	);

process
begin
  error <= '0';
  wait for 25 ns;
  clock <= not clock;
  finished <= '0';
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "01010101"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  if (shift /= '0') then
      error <= '1';
  end if;
  finished <= '0';
  
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "11110000"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "01011001"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  if (shift /= '0') then
      error <= '1';
  end if;
  finished <= '0';


  wait for 25 ns;
  clock <= not clock;
  scan_code <= "00010010"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  if (shift /= '1') then
      error <= '1';
  end if;
  finished <= '0';
  
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "10101010"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  if (shift /= '1') then
      error <= '1';
  end if;
  finished <= '0';
  
  
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "11110000"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  finished <= '0';
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "00010010";
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  if (shift /= '0') then
      error <= '1';
  end if;
  finished <= '0';
  

  wait for 25 ns;
  clock <= not clock;
  scan_code <= "01010101"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  if (shift /= '0') then
      error <= '1';
  end if;
  finished <= '0';
  

  wait for 25 ns;
  clock <= not clock;
  scan_code <= "01011001"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  if (shift /= '1') then
      error <= '1';
  end if;
  finished <= '0';
  

  wait for 25 ns;
  clock <= not clock;
  scan_code <= "01010101"; 
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  if (shift /= '1') then
      error <= '1';
  end if;
  finished <= '0';
  

  
if (error = '0') then
	   report "No error detected. Simulation successful" severity failure;
else
	    report "Error detected" severity failure;
end if;	
end process;
end arch;