library ieee;
use ieee.std_logic_1164.all;


entity scan_converter is
	port(
		shift	  : in std_logic;
		scan_code : in std_logic_vector(7 downto 0);
		ascii 	  : out std_logic_vector(7 downto 0)
		);
end scan_converter;

architecture arch of scan_converter is
	begin
		ascii <= "01000100" when shift ='1' and scan_code = "00100011" 		 -- 23 D
				 else "01100100" when shift ='0' and scan_code = "00100011"  -- 23 d
				 else "01000001" when shift = '1' and scan_code = "00011100" -- 1C A
				 else "01100001" when shift = '0' and scan_code = "00011100" -- 1C a
				 else "01010110" when shift = '1' and scan_code = "00101010" -- 2A V
				 else "01110110" when shift = '0' and scan_code = "00101010" -- 2A v
				 else "01000111" when shift = '1' and scan_code = "01000011" -- 43 I
				 else "01101001" when shift = '0' and scan_code = "01000011" -- 43 i
				 else "00100000" when scan_code = "00101001"				 -- 29 Space 
				 else "01001100" when shift = '1' and scan_code = "01001011" -- 4B L
				 else "01101100" when shift = '0' and scan_code = "01001011" -- 4B l
				 else "01000101" when shift = '1' and scan_code = "00100100" -- 24 E
				 else "01100101" when shift = '0' and scan_code = "00100100" -- 24 e
				 else "01001111" when shift = '1' and scan_code = "01000100" -- 44 O
				 else "01101111" when shift = '0' and scan_code = "01000100" -- 44 o
				 else "01001110" when shift = '1' and scan_code = "00110001" -- 31 N
				 else "01101110" when shift = '0' and scan_code = "00110001" -- 31 n
				 else "01010010" when shift = '1' and scan_code = "00101101" -- 2D R
				 else "01110010" when shift = '0' and scan_code = "00101101"; -- 2D r
end arch;