library ieee;
use ieee.std_logic_1164.all;

entity test_scan_converter is
end test_scan_converter;

architecture arch of test_scan_converter is
component scan_converter
port(
	shift : in std_logic;
	scan_code : in std_logic_vector(7 downto 0);
	ascii	: out std_logic_vector(7 downto 0)
	);
end component;

signal code : std_logic_vector(7 downto 0);
signal char : std_logic_vector(7 downto 0);
signal shift: std_logic;
signal error: std_logic := '0';

begin

SC: scan_converter port map(
	shift => shift,
	scan_code => code,
	ascii => char
	);
	process begin
	  --D
		shift <= '1';
		wait for 10 ns;
		code <= "00100011";
		wait for 10 ns;
		if(char = "01000100")then
			error <= '0';
		end if;
		
		wait for 200 ns;
		--a
		shift <= '0';
		wait for 10 ns;
		code <= "00011100";
		wait for 10 ns;
		if(char = "01100001") then
		  error <= '0';
		wait for 200 ns;
		end if;
		
		--v
		shift <= '0';
		wait for 10 ns;
		code <= "00101010";
		wait for 10 ns;
		if(char = "01110110") then
		  error <= '0';
		wait for 200 ns;
		--e
		end if;
		
		shift <= '0';
		wait for 10 ns;
		code <= "00100100";
		wait for 10 ns;
		if(char = "01100101") then
		  error <= '0';
		 wait for 200 ns;
		end if;
		
		--L
		shift <= '1';
		wait for 10 ns;
		code <= "01001011";
		wait for 10 ns;
		if(char = "01001100") then
		  error <= '0';
		 wait for 200 ns;
		
		end if;
		 
    
		if(error ='0') then
			report "No errors found. Simulation was successful!" severity failure;
		else
			report "Error(s) were found." severity failure;
		end if;
	end process;
end arch;