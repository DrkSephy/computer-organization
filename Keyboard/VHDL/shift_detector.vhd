library ieee;
use ieee.std_logic_1164.all;

entity shift_detector is
	port(
		clock		: in std_logic;
		finished	: in std_logic;
		scan_code	: in std_logic_vector(7 downto 0);
		shift		: out std_logic);
end shift_detector;

architecture arch of shift_detector is
	type state_type is (idle, pressed, released);
	signal current_state, next_state: state_type;
	
	begin  
		process(current_state, next_state)
			begin
				shift <= '0';
				case current_state is
					when idle =>
						shift <= '0';
						if((scan_code = "00010010" or scan_code = "01011001") and finished = '1') then
							next_state <= pressed;
						else
							next_state <= idle;
						end if;
						
				    when pressed =>
				    	next_state <= pressed;
						shift <= '1';
						if(scan_code = "11110000" and finished = '1')then
							next_state <= released;
						end if;
						
						
					when released =>
						next_state <= released;
						shift <= '1';
						if(finished = '1')then
							if(scan_code = "00010010" or scan_code = "01011001")then
						--system restarts process from idle state
								--shift <= '1';
								next_state <= idle;
							else
						--go back to pressed state and wait for second shift key
						--to be pressed
								next_state <= pressed;
							end if;
						end if;
				end case;
		  end process;
				
				--to go from state to state, we'll use a clock to transition
		process(clock)
			begin
				if(rising_edge(clock)) then
					current_state <= next_state;
				end if;
    end process;
end arch;