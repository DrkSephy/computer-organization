library ieee;
use ieee.std_logic_1164.all;

entity shift_detector is
port(
	Clock		: in std_logic;
	finished	: in std_logic;
	scan_code	: in std_logic_vector(7 downto 0);
	shift		: out std_logic
);
end shift_detector;

architecture behav of shift_detector is
	type state_type is (idle, left_shift, break_left_shift, 
	right_shifted, break_right_shifted, both_shifts,
	break_both_shifts, break_idle);
	signal current_state: state_type := idle;
	signal next_state: state_type := idle;
begin

	process (scan_code, current_state)
	begin
		case current_state is
			
			when idle =>
				if scan_code = "11110000" then
					next_state <= break_idle;
				elsif scan_code = "00010010" then
					next_state <= left_shift;
				elsif scan_code = "01011001" then
					next_state <= right_shifted;
				else
					next_state <= idle;
				end if;
				shift <= '0';
			when left_shift =>
				if scan_code = "11110000" then
					next_state <= break_left_shift;
				elsif scan_code = "01011001" then
					next_state <= both_shifts;
				else
					next_state <= left_shift;
				end if;
				shift <= '1';
			
			when break_left_shift =>
				if scan_code = "00010010" then
					next_state <= idle;
				else
					next_state <= left_shift;
				end if;
				shift <= '1';
			
			when right_shifted =>
				if scan_code = "11110000" then
					next_state <= break_right_shifted;
				elsif scan_code = "00010010" then
					next_state <= both_shifts;
				else
					next_state <= right_shifted;
				end if;
				shift <= '1';
			when break_right_shifted =>
				if scan_code = "01011001" then
					next_state <= idle;
				else
					next_state <= right_shifted;
				end if;
				shift <= '1';
			
			when both_shifts =>
				if scan_code = "11110000" then
					next_state <= break_both_shifts;
				else
					next_state <= both_shifts;
				end if;
				shift <= '1';
			
			when break_both_shifts =>
				if scan_code = "00010010" then
					next_state <= right_shifted;
				elsif scan_code = "01011001" then
					next_state <= left_shift;
				else
					next_state <= both_shifts;
				end if;
				shift <= '1';
			
			when break_idle =>
				next_state <= idle;
				shift <= '0';
		end case;
	end process;
end behav;