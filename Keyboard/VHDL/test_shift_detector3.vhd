library ieee;
use ieee.std_logic_1164.all;

entity Test_shift_detector is
end Test_shift_detector;

Architecture arch_test of Test_shift_detector is

component shift_detector port(

clock : in std_logic;
finished : in std_logic;
scan_code : in std_logic_vector(7 downto 0);
shift : out std_logic
);
end component;

Signal clock: std_logic := '0';
Signal finished, shift, error: std_logic;
Signal scan_code: std_logic_vector(7 downto 0);-- scan_code

-- Left shift: 12h == 00010010
-- Right shift: 59h == 01011001
-- break : F0h == 11110000

begin

sd: shift_detector port map
(
	clock => clock,
  	finished => finished,
  	scan_code => scan_code,
  	shift => shift
	);

process
begin
  error <= '0';
  wait for 25 ns;
  clock <= not clock;
  finished <= '0';
  wait for 25 ns;
  clock <= not clock;
  -- Circuit does not indicate shift in response to non-shift codes 
  wait for 25 ns;
  scan_code <= "00110011"; -- non-shift code
  clock <= not clock;
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  wait for 25 ns;
  if (shift /= '0') then
      error <= '1';
  end if;
  finished <= '0';

  -- Circuit indicates shift in response to l_shift code
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "00010010"; -- left-shift code
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  wait for 25 ns;
  if (shift /= '1') then
      error <= '1';
  end if;
  finished <= '0';
  
  -- Circuit continues to indicate shift while other codes are sent before the break/l_shift combo
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "00110011"; -- other non-shift code
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  wait for 25 ns;
  if (shift /= '1') then
      error <= '1';
  end if;
  finished <= '0';
  
  -- Circuit does not indicate shift in response to break code followed by l_shift code
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "11110000"; -- break code
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  finished <= '0';
  scan_code <= "00010010"; -- left shift code
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  wait for 25 ns;
  if (shift /= '0') then
      error <= '1';
  end if;
  finished <= '0';
  
  -- Circuit then still does not indicate shift in response to non-shift codes
  wait for 25 ns;
  scan_code <= "00110011"; -- non-shift code
  clock <= not clock;
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  wait for 25 ns;
  if (shift /= '0') then
      error <= '1';
  end if;
  finished <= '0';
  
  -- Circuit indicates shift in response to r_shift code
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "01011001"; -- right-shift code
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  wait for 25 ns;
  if (shift /= '1') then
      error <= '1';
  end if;
  finished <= '0';
  
  -- Circuit continues to indicate shift while other codes are sent before the break/r_shift combo
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "00110011"; -- other non-shift code
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  wait for 25 ns;
  if (shift /= '1') then
      error <= '1';
  end if;
  finished <= '0';
  
  -- Circuit does not indicate shift in response to break code followed by r_shift code
  wait for 25 ns;
  clock <= not clock;
  scan_code <= "11110000"; -- break code
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  finished <= '0';
  scan_code <= "01011001"; -- right shift code
  wait for 25 ns;
  clock <= not clock;
  finished <= '1';
  wait for 25 ns;
  clock <= not clock;
  wait for 25 ns;
  if (shift /= '0') then
      error <= '1';
  end if;
  finished <= '0';
  
if (error = '0') then
	   report "No error detected. Simulation successful" severity failure;
	else
	    report "Error detected" severity failure;
end if;	
end process;
end arch_test;